package apiAssignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

public class Put{
  @Test
  public void testPutOperation() {
	  
	  JSONObject req = new JSONObject();
	  req.put("name", "Shyam");
	  req.put("email", "shyamsundar.tech24@gmail.com");
	  req.put("gender", "male");
	  req.put("status", "Active");
		
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
		.header("authorization", "Bearer 956bbfbcab7df0694ce4e254606baefcab2afa8c02c7bdeb444f018ddd360ed2")
		.body(req.toJSONString()).patch("/users/190199").then().statusCode(200);
 	  
  }
}
