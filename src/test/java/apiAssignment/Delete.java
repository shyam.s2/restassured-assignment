package apiAssignment;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.annotations.Test;

public class Delete {
	@Test
	public void f() {
		
		baseURI = "https://gorest.co.in/public/v2";
		given().log().all().contentType("application/json")
				.header("authorization", "Bearer 956bbfbcab7df0694ce4e254606baefcab2afa8c02c7bdeb444f018ddd360ed2")
				.delete("/users/190199").then().statusCode(204);
	}
}
