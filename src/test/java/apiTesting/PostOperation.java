package apiTesting;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class PostOperation {
  @Test
  public void postOperationTest() {
	  JSONObject request = new JSONObject();
	  request.put("name", "Shyam");
	  request.put("job", "trainee");
	  System.out.println(request);
	  baseURI="https://reqres.in/api";
	  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
	  
  }
}
