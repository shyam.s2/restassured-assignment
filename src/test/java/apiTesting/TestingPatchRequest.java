package apiTesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import org.json.simple.JSONObject;
public class TestingPatchRequest {
  @Test
  public void updatingDataUsingPatch() {
	  JSONObject request= new JSONObject();
		 request.put("name", "shyam");
		 request.put("job", "trainee");
		 
		 given().body(request.toJSONString()).patch("https://reqres.in/api/users/2").then().statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:50:12.293Z"));
		 
		 
  }
}
