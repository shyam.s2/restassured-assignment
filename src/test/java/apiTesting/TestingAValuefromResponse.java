package apiTesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class TestingAValuefromResponse {
  @Test
  public void testingvalue() {
	  baseURI="https://reqres.in";
	  given().get("api/users?page=2").then().body("data.first_name", hasItem("Byron"));
	    }
@Test
public void testingMultipleValues()
{
	baseURI="https://reqres.in";
	  given().get("api/users?page=2").then().body("data.first_name", hasItems("Byron","Michael")).body("data.last_name", hasItem("Lawson"));
}

}
