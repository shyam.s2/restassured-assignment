package apiTesting;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
public class TestingGetUsingGivenWhenApproach {
  @Test
  public void testStatusCode() {
baseURI="https://reqres.in";
given().get("/api/users/2").then().statusCode(200);
}
  @Test
  public void testparticularvalue() {
	  baseURI="https://reqres.in";
	  given().get("/api/users/2").then().body("data.last_name", equalTo("Weaver"));	  
  }
  @Test
  public void printAllValues() {
	  baseURI="https://reqres.in";
	  given().get("/api/users/2").then().log().all();
  }
}
